import { Component, ElementRef, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-main-container',
  templateUrl: './main-container.component.html',
  styleUrls: ['./main-container.component.css']
})
export class MainContainerComponent {
  @ViewChild('popUp', { static: false }) popUp: ElementRef | undefined;
  private modalReference: any;
  constructor(private modalService: NgbModal) { }
  active = 1;
  private checkBoxListTab2: any = [{
    id: 1,
    title: 'Option 1',
    checked: false,
  },
  {
    id: 2,
    title: 'Option 2',
    checked: false,
  },
  {
    id: 3,
    title: 'Option 3',
    checked: false,
  },
  {
    id: 4,
    title: 'Option 4',
    checked: false,
  }]
  private checkBoxListTab4: any = [{
    id: 1,
    title: 'Option 1',
    checked: false,
  },
  {
    id: 2,
    title: 'Option 2',
    checked: false,
  },
  {
    id: 3,
    title: 'Option 3',
    checked: false,
  },
  {
    id: 4,
    title: 'Option 4',
    checked: false,
  }]
  public radioList: any = [
    {
      answerOptionId: 1,
      answerText: 'Male'
    },
    {
      answerOptionId: 2,
      answerText: 'Female'
    }
  ]
  public tabValues: any = {
    tab1: { input: null, dropdown: null, list: null, radioList: null },
    tab2: { input: null, dropdown: null, list: this.checkBoxListTab2, radioList: null },
    tab3: { input: null, dropdown: null, list: null, radioList: null },
    tab4: { input: null, dropdown: null, list: this.checkBoxListTab4, radioList: null },
    tab5: { input: null, dropdown: null, list: null, radioList: null }
  };
  public dropDownList: string[] = ["Year", "Title", "Author"];


  openTab(id: number) { this.active = id; }

  onSubmit() {
    this.OpenPopup(this.popUp);
    console.log(this.tabValues)
  }

  public OpenPopup(content: any) {
    this.modalReference = this.modalService.open(content, { centered: true });
  }
  public CloseModal() {
    this.modalReference ? this.modalReference.close() : "";
  }
  public inputValues(evt: any, tab: number) {
    switch (tab) {
      case 2:
        this.tabValues.tab2 = evt
        break;
      case 4:
        this.tabValues.tab4 = evt
        break;
    }

  }

  get tab2result() {
    return this.tabValues.tab2.list.filter((item: any) => item.checked);
  }
  get tab4result() {
    return this.tabValues.tab4.list.filter((item: any) => item.checked);
  }
}

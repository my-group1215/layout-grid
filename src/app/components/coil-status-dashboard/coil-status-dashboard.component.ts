import { Component } from '@angular/core';

@Component({
  selector: 'app-coil-status-dashboard',
  templateUrl: './coil-status-dashboard.component.html',
  styleUrls: ['./coil-status-dashboard.component.scss']
})
export class CoilStatusDashboardComponent {

  trackingStatus: any;

  reset(){
    this.trackingStatus = 0;
  }
}

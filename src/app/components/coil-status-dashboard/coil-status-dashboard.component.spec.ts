import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoilStatusDashboardComponent } from './coil-status-dashboard.component';

describe('CoilStatusDashboardComponent', () => {
  let component: CoilStatusDashboardComponent;
  let fixture: ComponentFixture<CoilStatusDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CoilStatusDashboardComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CoilStatusDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

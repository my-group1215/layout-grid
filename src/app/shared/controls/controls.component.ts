import { Component, EventEmitter, Input, Output } from '@angular/core';
@Component({
  selector: 'my-app-controls',
  templateUrl: './controls.component.html',
  styleUrls: ['./controls.component.css']
})
export class ControlsComponent {
  @Input() values: any = { input: '', dropdown: 'DropDown', list: [], radioOption: [] };
  @Input() dropDownList: string[] = [];
  @Input() radioList: any[] = [];
  @Output() inpValue = new EventEmitter();

  public updateValue() {
    this.inpValue.emit(this.values);
  }
  ChangeSortOrder(newSortOrder: string) {
    this.values.dropdown = newSortOrder;
    this.inpValue.emit(this.values);
  }



  handleChange(option: any) {
    this.values.radioOption = option;
    this.inpValue.emit(this.values);
  }
}

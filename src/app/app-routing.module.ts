import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CoilStatusDashboardComponent } from './components/coil-status-dashboard/coil-status-dashboard.component';
import { MainContainerComponent } from './components/main-container/main-container.component';

const routes: Routes = [
  {
    path: '',
    component: MainContainerComponent,
  },
  {
    path: 'planning/coil-dashboard',
    component: CoilStatusDashboardComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
